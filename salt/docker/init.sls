{%- from "vars" import vars with context %}

docker-repo:
  pkgrepo.managed:
    - name: deb https://download.docker.com/linux/{{ grains['os'] | lower }} {{ grains['oscodename'] }} stable
    - file: /etc/apt/sources.list.d/docker.list
    - key_url: https://download.docker.com/linux/{{ grains['os'] | lower }}/gpg
    - clean_file: True
    - require_in:
        - pkg: docker-ce

docker-ce:
  pkg.installed

docker-compose:
  pkg.installed
