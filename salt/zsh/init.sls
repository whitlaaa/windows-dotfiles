{%- from "vars" import vars with context %}

zsh:
  pkg.installed

oh-my-zsh:
  cmd.run:
    - name: sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    - require:
      - pkg: zsh

zshrc:
  file.managed:
    - name: {{ vars.home_dir }}/.zshrc
    - source: salt://zsh/.zshrc

aliases:
  file.managed:
    - name: {{ vars.home_dir }}/aliases.zsh
    - source: salt://zsh/aliases.zsh

path:
  file.managed:
    - name: {{ vars.home_dir }}/path.zsh
    - source: salt://zsh/path.zsh

dircolors:
  file.managed:
    - name: {{ vars.home_dir}}/.dircolors
    - source: https://raw.githubusercontent.com/seebi/dircolors-solarized/master/dircolors.256dark
    - skip_verify: True
    - mode: 755

zsh-syntax-highlighting:
  git.latest:
    - name: https://github.com/zsh-users/zsh-syntax-highlighting.git
    - target: {{ vars.home_dir }}/.oh-my-zsh/plugins/zsh-syntax-highlighting
    - user: {{ vars.username }}
    - force_fetch: True
    - force_reset: True

zsh-default-shell:
  file.append:
    - name: {{ vars.home_dir }}/.bashrc
    - text: |
        if [ -t 1 ]; then
          exec zsh
        fi
