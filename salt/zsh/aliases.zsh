# Shortcuts
alias reloadcli="source $HOME/.zshrc"
alias weather="curl -4 http://wttr.in"
alias edithosts='powershell.exe -Command "Start-Process notepad.exe C:\Windows\system32\drivers\etc\hosts -Verb RunAs"'
alias ps-admin='powershell.exe -Command "Start-Process PowerShell –Verb RunAs"'

# Directories
alias downloads="cd /mnt/c/Users/abwhitler/Downloads"
alias workspace="cd /mnt/c/dev/workspace"
