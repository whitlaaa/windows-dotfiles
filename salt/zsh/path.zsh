# Load Node global installed binaries
export PATH="$HOME/.node/bin:$PATH"

# Load Yarn global installed binaries
export PATH="$HOME/.yarn/bin:$PATH"

# Use project specific binaries before global ones
export PATH="node_modules/.bin:vendor/bin:$PATH"

# Local bin directories before anything else
export PATH="/usr/local/bin:/usr/local/sbin:$PATH"

# Use Docker Windows host
export DOCKER_HOST=tcp://0.0.0.0:2375

# Java/Maven
# export JAVA_HOME=/path/to/jdk
# export MAVEN_HOME=/path/to/maven
# export M2_HOME=/path/to/maven
