oracle-ppa:
  pkgrepo.managed:
    - humanname: WebUpd8 Oracle Java PPA repository
    - ppa: webupd8team/java

debconf-utils:
  pkg.installed

oracle-java-accept-license:
  debconf.set:
    - name: oracle-java8-installer
    - data:
        'shared/accepted-oracle-license-v1-1': {'type': 'boolean', 'value': 'true' }
    - require:
      - pkg: debconf-utils

oracle-java8-installer:
  pkg:
    - installed
    - require:
      - pkgrepo: oracle-ppa
      - debconf: oracle-java-accept-license
