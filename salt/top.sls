base:
  '*':
    - git
    - zsh
    - terminal
    - nodejs
    - yarn
    - docker
    - go
#    - java
#    - maven
    - k8s
    - terraform
#    - azure
    - general
