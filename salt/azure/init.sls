azure-repo:
  pkgrepo.managed:
    - humanname: Azure CLI
    - name: deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli wheezy main
    - file: /etc/apt/sources.list.d/azure-cli.list
    - keyserver: packages.microsoft.com
    - keyid: '52E16F86FEE04B979B07E28DB02C46DF417A0893'
    - require_in:
      - pkg: azure-cli

azure-cli:
  pkg.installed:
    - pkgs:
        - apt-transport-https
        - azure-cli
