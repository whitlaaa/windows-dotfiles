kubectl:
  cmd.script:
    - name: salt://k8s/install-kubectl.sh

helm:
  cmd.script:
    - name: https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get
