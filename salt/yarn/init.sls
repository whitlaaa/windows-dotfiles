{%- from "vars" import vars with context %}

yarn-repo:
  pkgrepo.managed:
    - humanname: Yarn
    - name: deb https://dl.yarnpkg.com/debian/ stable main
    - file: /etc/apt/sources.list.d/yarn.list
    - key_url: https://dl.yarnpkg.com/debian/pubkey.gpg
    - require_in:
      - pkg: yarn

yarn:
  pkg.installed
