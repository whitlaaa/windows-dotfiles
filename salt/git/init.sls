{%- from "vars" import vars with context %}

git:
  pkg.installed

gitconfig_global:
  file.managed:
    - name: {{ vars.home_dir }}/.gitconfig
    - source: salt:///git/.gitconfig_global

gitignore_global:
  file.managed:
    - name: {{ vars.home_dir }}/.gitignore_global
    - source: salt:///git/.gitignore_global
