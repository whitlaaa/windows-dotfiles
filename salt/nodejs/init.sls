nodejs-repo:
  pkgrepo.managed:
    - humanname: NodeJS
    - name: deb https://deb.nodesource.com/node_8.x {{ grains['oscodename'] }} main
    - file: /etc/apt/sources.list.d/nodesource.list
    - keyserver: keyserver.{{ grains['os'] }}.com
    - keyid: '68576280'
    - require_in:
      - pkg: nodejs

nodejs:
  pkg.installed
