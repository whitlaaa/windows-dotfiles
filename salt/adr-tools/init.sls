{%- from "vars" import vars with context %}

adr-tools:
  archive.extracted:
    - name: {{ vars.home_dir }}/dev
    - source: https://github.com/npryce/adr-tools/archive/2.1.0.tar.gz
    - skip_verify: True

adr-tools-path:
  file.append:
    - name: {{ vars.home_dir }}/path.zsh
    - text: |
        # adr-tools (https://github.com/npryce/adr-tools/)
        export PATH="$HOME/dev/adr-tools-2.1.0/src:$PATH"
