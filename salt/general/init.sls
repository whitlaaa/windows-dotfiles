{%- from "vars" import vars with context %}

packages:
  pkg.installed:
    - pkgs:
      - htop
      - subversion
      - tree
      - unzip
      - zip

create_windows_workspace:
  file.directory:
    - name: {{ vars.win_workspace }}
    - makedirs: True

home-directory-ownership:
  cmd.run:
    - name: chown -R {{ vars.username }}:{{ vars.username }} {{ vars.home_dir }}
