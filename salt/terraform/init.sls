extract-terraform:
  archive.extracted:
    - name: /usr/local/bin
    - source: https://releases.hashicorp.com/terraform/0.11.7/terraform_0.11.7_linux_amd64.zip
    - skip_verify: True
    - enforce_toplevel: False
