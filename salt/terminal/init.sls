{%- from "vars" import vars with context %}

tmux:
  pkg.installed

byobu:
  pkg.installed

byobu-shell:
  file.append:
    - name: {{ vars.home_dir }}/.byobu/.tmux.conf
    - text: |
        set -g default-shell /usr/bin/zsh
        set -g default-command /usr/bin/zsh
    - makedirs: True
    - require:
      - pkg: byobu

hyperjs-config:
  file.managed:
    - name: {{ vars.win_home_dir }}/.hyper.js
    - source: salt://terminal/.hyper.js
