# Alex's janky Windows dotfiles

This setup uses [SaltStack](https://saltstack.com/) to configure WSL (along with a couple of Windows-side elements since it was convenient),
as well as [Chocolatey](https://chocolatey.github.io/) to install components on the Windows side.

## Windows Subsystem for Linux

### Install

Follow instructions [here](https://docs.microsoft.com/en-us/windows/wsl/install-win10)

### Setup

* Install [UbuntuMono NF](https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/UbuntuMono/Regular/complete/Ubuntu%20Mono%20Nerd%20Font%20Complete%20Mono%20Windows%20Compatible.ttf)
  * Set as font in `Bash for Windows` (or terminal emulator of choice; more detail in `Windows` section below)
* Install SSH keys (`~/.ssh`)

#### SaltStack on WSL

Install SaltStack via the bootstrap script:

```shell
curl -o bootstrap-salt.sh -L https://bootstrap.saltstack.com
sudo sh bootstrap-salt.sh git develop
```

Apply highstate from root of project:

```shell
sudo salt-call --local --file-root=./salt state.apply
```

This will take a little while, so starting the installs on the Windows side while we wait will save some time.

## Windows

* Install `chocolatey` in a Powershell admin window:
  * `Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))`
* Install primary apps with `choco install windows/choco-packages.config --yes`
  * This will install everything defined in `windows/choco-packages.config` so will take some time
* The above step will install the [Hyper](https://hyper.is/) terminal emulator, but another good alternative is [wsl-terminal](https://github.com/goreliu/wsl-terminal)
  * To install wsl-terminal run `windows/wsl-terminal.ps1` (Borrowed from [Jessica Deen's setup](https://github.com/jldeen/dotfiles/tree/wsl))
  * Some extra tweaks:
    * Font = UbuntuMono NF 13pt
    * Theme = google-dark
    * Set `shell=byobu` in `wsl-terminal.conf`

### Docker for Windows

* Check box to expose via TCP
* Share `C:`
* Up RAM to 4GB

### VSCode

All Visual Studio Code settings, keybindings, and extensions are synced via the [Settings Sync Extension](https://marketplace.visualstudio.com/items?itemName=Shan.code-settings-sync). This [gist](https://gist.github.com/whitlaaa/edd824ee2034ed08b5f79d5506af11ac) holds my settings.

### mRemoteNG

* Install connections file wherever

### Additional Features

Enable some other items under `Turn Windows features on or off`. In PowerShell as admin:

```powershell
./windows/enable-features.ps1
```

* `Remote Server Administration Tools` isn't available as an optional feature, and doesn't work properly with the Creator's Update, so it needs some special attention.
  * Follow the steps [here](https://support.microsoft.com/en-us/help/4055558/rsat-missing-dns-server-tool-in-windows-10-version-1709)
